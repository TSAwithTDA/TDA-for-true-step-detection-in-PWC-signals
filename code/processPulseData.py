import numpy as np
# import TSAwithTDA.pyPerseus as pP
import matplotlib.pyplot as plt
from scipy.io import loadmat
from scipy.fftpack import fft, ifft
import os.path
from os import listdir
from os.path import isfile, join
import re
import pandas as pd
import scipy
import scipy.signal
import scipy.io
import time
import platform
import seaborn as sns

sns.set(font_scale=1.5)  # switch to seaborn defaults
sns.set_style("ticks")


# this file plots the necessary pictures for the laser tach paper

import os.path
import matplotlib
matplotlib.style.use('ggplot')

import matplotlib.pyplot as plt
from scipy.stats import ttest_1samp, ttest_rel, normaltest
from mpl_toolkits.axes_grid import inset_locator
from mpl_toolkits.axes_grid1.inset_locator import TransformedBbox, BboxPatch, BboxConnector
import matplotlib
matplotlib.style.use('ggplot')

# from matplotlib.colors import LogNorm
import seaborn as sns
sns.set(font_scale=1.5)  # switch to seaborn defaults
sns.set_style("ticks")

# setting plotting style to latex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

# draw a bbox of the region of the inset axes in the parent axes and
# connecting lines between the bbox and the inset axes area
# loc1, loc2 : {1, 2, 3, 4}
def mark_inset(parent_axes, inset_axes, loc1a=1, loc1b=1, loc2a=2, loc2b=2, **kwargs):
    rect = TransformedBbox(inset_axes.viewLim, parent_axes.transData)

    pp = BboxPatch(rect, fill=False, **kwargs)
    parent_axes.add_patch(pp)

    p1 = BboxConnector(inset_axes.bbox, rect, loc1=loc1a, loc2=loc1b, **kwargs)
    inset_axes.add_patch(p1)
    p1.set_clip_on(False)
    p2 = BboxConnector(inset_axes.bbox, rect, loc1=loc2a, loc2=loc2b, **kwargs)
    inset_axes.add_patch(p2)
    p2.set_clip_on(False)

    return pp, p1, p2


# this function extracts the nominal RPM and the trial number from the file name
def get_nominal_rpm_and_trialno(fileName):
    # use regular expressions to extract the nominal rpms and the trial numbers from the file names
    # F_29-Jun-2017_tachTest_nominal_30RPM_trial1
    rpm_pattern = re.compile('(\\d*)RPM.*trial(\\d*)', re.IGNORECASE)
    nominal_rpm = rpm_pattern.search(fileName).group(1)
    trial_no = rpm_pattern.search(fileName).group(2)
    return nominal_rpm, trial_no


# this function uses hard thresholdikng to filter the data
def hard_threshold(folder_to_load, file_name, low_level, high_level):
    file = loadmat(os.path.join(folder_to_load, file_name))
    t = file['t']
    # extract
    y_raw = file['d'][:, 1]

    # get rid of the noise
    threshold = 2.5  # threshold for on-off in digital signal

    y = np.copy(y_raw)  # get a copy of the signal

    # now use hard-thresholding
    y[y < threshold] = low_level
    y[y >= threshold] = high_level

    return t, y


# this function finds the rpm using persistence (th distribution of differences)
def persistence_rpm(t, y, high_level=1):

    # reshape y and t so that they're rows
    y = np.reshape(y, (1, -1))
    t = np.reshape(t, (1, -1))

    # find the location of all the peaks
    tpeaks = t[y>= high_level/2]

    tdiff = np.diff(tpeaks)

    # find the differences in the sorted differences
    tdiff_unsorted = np.copy(tdiff)  # unsorted tdiff

    # sort tdiff
    tdiff.sort()  # sorted tdiff

    # find the difference in differences
    tdiffdiff = np.diff(tdiff)

    # find the index where tdiffdiff is maximum
    ind_max_time = np.where(tdiffdiff == max(tdiffdiff))

    # get the number of full periods
    number_of_full_periods = len(tdiff) - (ind_max_time[0][0] + 1)

    if number_of_full_periods == 1:
        # print('Only one space is counted, so persistence RPM will fail.')
        # print('...Returning NaN')
        return np.NaN

    # print(number_of_full_periods)

    # find time vector end points
    # first, find time of the second peak (dropping the first peak regardless whether it was partial or not)
    whereMaxOccurs = np.where(tdiffdiff == max(tdiffdiff))[0][0]


    # threshold for finding the first and last rising peaks
    mu = tdiff[whereMaxOccurs] + 0.5 * max(tdiffdiff)  


    # find the first rising edge
    t_left = tpeaks[np.where(tdiff_unsorted > mu)[0]+1][0]  
    # print(t_left)

    # second, find time of the second to last peak (dropping the last peak regardless whether it was partial or not)
    t_right = tpeaks[np.where(tdiff_unsorted > mu)[0]+1][-1]  # find the last rising edge
    # print(t_right)
    # print('iter:', iter, ', t_right:', t_right, ',t_left:',t_left)


    # get the rpm
    rpm = (number_of_full_periods-1) / (t_right - t_left) * 60  # rev/minutes

    # if np.isnan(rpm):
    #     print('The number of voids counted is', number_of_full_periods)
    #     plt.hist(tdiff)
    #     plt.show()

    # print(rpm)

    return rpm


# ---- use Fourier transform
def fourier_rpm(t, y):
    # reshape y and t so that they're rows
    # y = np.reshape(y, (1, -1))
    # t = np.reshape(t, (1, -1))
    # remove linear component of the signal
    y = y - np.mean(y)

    # get the sampling interval
    # t_sampling = (t[1] - t[0])[0]  # sampling interval needed when loading from files
    t_sampling = t[1] - t[0]  # sampling interval when using synthetic data
    # get the sampling rate
    freq_sampling = 1 / t_sampling  # sampling rate
    signal_length = t.shape[0]  # length of the signal

    # get the FFT of the signal
    Y = fft(y)

    # get the one sided spectrum
    one_sided_spectrum = 2.0/signal_length * np.abs(Y[0:signal_length//2])

    # zero out the 0 Hz coefficient (the DC component)

    # define the frequency vector
    freq = np.linspace(0.0, 1.0/(2.0*t_sampling), signal_length//2)

    # # plot Fourier
    # plt.plot(freq, one_sided_spectrum)
    # plt.grid()
    # # plt.savefig('foo.png')
    # plt.show()

    # find the value and the index of the first peak in fft
    ind = np.where(one_sided_spectrum >= np.amax(one_sided_spectrum)/3)

    # get the rpm as the frequency where the maximum peak occurs in the spectrum
    rpm_fourier = freq[ind[0][0]] * 60  # rpm

    # # print(freq)
    # # print(t.shape)
    # # print(freq.shape)
    # # print(sp.shape)
    #

    return rpm_fourier


#---------------------------------------------------------#
#---------------------------------------------------------#
#---Code to generate example time series------------------#
#---------------------------------------------------------#
#---------------------------------------------------------#


#------------Uniform Noise--------------------------------#
# define uniform noise functions
def uni(length, halfHeight = 1, seed = None):
    # Returns a vector of length `length` drawn uniformly from 
    np.random.seed(seed)
    noise = np.random.rand(length)*2*halfHeight-halfHeight
    return noise


def uniPos(length, Height = 1,seed = None):
    np.random.seed(seed)
    noise = np.random.rand(length)*Height
    return noise

#------------Pulses---------------------------------------#


def pulse(t,T=1,duty = .1, max_pulse_height = 1):
    '''
    Creates a pulse wave with period T.

    Function values are:
    | max_pulse_height    if t mod T < T*duty
    |     0               else
    '''
    y = max_pulse_height * .5 *(scipy.signal.square(2*np.pi/T*t,duty = duty)+1)

    return y

def pulse_with_noise(t,T, duty, alpha_percent = 0, beta_percent =0, epsilon_percent=0, seed = None, max_pulse_height = 1):
    '''
    Creates a pulse with alpha, beta, and epsilon noise present.  See paper for details.
    Note that this assumes these values are given as a percentage of other parameters.
    Reasonable options for each:
      - alpha_percent is a percentage of tau, and gives the digital noise inputs.
        This can be [0,.5].
      - beta_percent is the y-noise given as a percentage of the amplitude
    '''
    np.random.seed(seed)

    # find the width of the spike
    tau = duty * T


    # noise to add in the x-direction (causes digital noise)
    alpha = tau * alpha_percent

    # noise to add in the y-direction
    beta = max_pulse_height * beta_percent

    # accordian noise
    epsilon = epsilon_percent * T  

    if epsilon_percent>0:
        # initialize Q
        Q = []
        while sum(Q) < t[-1]:
            # get the list of random period lengths drawn from a uniform distribution
            # on [T-epsilon, T+epsilon]
            Q.extend(T + uni(10, epsilon))

        # Find the locations of the beginning of each period
        C = [sum(Q[:i]) for i in range(len(Q)+1)]
        C = np.array(C)

        def getPeriodIndex(x):
            if x == 0:
                return 0
            else:
                return np.where(C<x)[0][-1]

        # Tag each entry in the time vector t by which period it belongs to
        index = [getPeriodIndex(x) for x in list(t)]

        # phi respaces the real line to match the period
        #phi = [T*i+(T/Q[i])*(t[j] - C[i]) for j,i in enumerate(index)]
        phi = [(T/Q[i])*(t[j] - C[i]) for j,i in enumerate(index)]
        phi = np.array(phi)

    else:
        phi = t

    # Add digital noise
    if alpha_percent > 0:
        phi = phi+uni(len(phi), halfHeight=alpha, seed=seed)



    # 
    out = pulse(phi, T=T, duty = duty, max_pulse_height = max_pulse_height)

    if beta_percent > 0:
        out += uni(len(out), halfHeight=beta, seed=seed)

    return out



#------------------------------------------------------#
#------------------------------------------------------#
#----------Making the big data frames------------------#
#------------------------------------------------------#
#------------------------------------------------------#

def makeBigDataFrame_EpsVsT(nominal_RPM_vec = np.linspace(30, 24000, 100),
                     eps_ratio_vec = np.linspace(0.02, 0.65, 100),
                     num_replicates = 100,
                     oversample_factor = 32, 
                     num_periods = 32,
                     duty = .05,
                     alpha_percent = 0.1,
                     starting_seed = 48824
                    ):
    '''
    Makes the first big data frame for the paper

    nominal_RPM_vec
        A vector giving the nominal RPMs to be used
    eps_ratio_vec
        A vector giving the epsilons to be used.  These will be interpreted as a percentage
        of the period T
    num_replicates
        The number of times to repeat each experiment
    oversample_facture
        Generate this times the number of samples needed by Shannon
    num_periods
        The number of periods to generate

    '''
    # paper, long calculations



    # define the nominal rpm and T vectors
    # nominal_RPM_vec = np.linspace(30, 24000, 200)  # rev/min
    T_vec = 60 / nominal_RPM_vec  # sec/rev

    # get all the combinations using meshgrid
    X_eps, Y_T = np.meshgrid(eps_ratio_vec, T_vec)

    # initialize a Pandas data frame that will hold the output from the first for loop
    colNames3 = ['seed_no', 
                 'alpha_percent', 
                 'eps_over_T', 
                 'nominal_rpm', 
                 'rpm_pers', 
                 'rpm_fourier',
                 'time_pers', 
                 'time_fourier']
    eps_rpm_df = pd.DataFrame(columns=colNames3,
                              index=np.arange(num_replicates * X_eps.size), dtype=np.float64)


    # noise level in the y-direction (unused in this paper)
    beta_percent = 0
    max_pulse_height = 1 


    xmin = 1  # left bound of the time interval

    # define an index for the inner for loop. This will be used to increment the counter that stores the results of the
    # nested iteration
    ind1 = 0

    seed = starting_seed

    # create a data frame for the iteration over epsilon vector, and rpm vector with num_replicates
    for rep in range(num_replicates):
        # increment the seed for each replicate
        seed += 1

        # first loop is for the T, epsilon combinations
        for eps_percent, T in zip(np.ravel(X_eps), np.ravel(Y_T)):



            # find the right bound of the time interval
            xmax = xmin + num_periods * T

            # find the number of samples
            N = int(np.ceil(1/T * max(oversample_factor, 2) * (xmax-xmin)))

            # get the nominal rpm
            nominalRPM = 1.0/T * 60


            # initialize the time vector
            t = np.linspace(xmin, xmax, N)


            # Return the random pulse
            out_noise = pulse_with_noise(t,T, duty, 
                                alpha_percent = alpha_percent, 
                                beta_percent =beta_percent, 
                                epsilon_percent=eps_percent, 
                                seed = seed, 
                                max_pulse_height = max_pulse_height)

            # force the beginning and end to be zero
            out_noise[0] = 0
            out_noise[-1] = 0

            # find the rpm using persistence
            start = time.perf_counter()
            rpm_pers = persistence_rpm(t, out_noise)
            end = time.perf_counter()
            pers_time = end - start

            # find rpm using fourier
            # find the index for the right bound of t
            t_right_ind = np.where(t >= (xmax - xmin))[0][0]

            # get the rpm using Fourier
            start = time.perf_counter()
            rpm_fourier = fourier_rpm(t, out_noise)
            end = time.perf_counter()
            fourier_time = end - start

            # print(pers_time - fourier_time)
            # if np.isnan(rpm_pers):
            #     print('yup')
            #     plt.plot(t,out)
            #     plt.show()
            # print(rep, eps_percent, T)
            # eps_rpm_df[rep, ] = ([rep, eps_percent, nominalRPM, rpm_pers, rpm_fourier, pers_time, fourier_time])
            # print(iter, rpm_fourier)
            # print(rpm_pers)
            #
            eps_rpm_df.iloc[ind1] = [int(rep), float(alpha_percent), float(eps_percent), float(nominalRPM),
                                    float(rpm_pers), float(rpm_fourier), float(pers_time), float(fourier_time)]

            # show progress
            print("Progress of loop 1 = {0:.2%}". format((rep + ind1)/(num_replicates * X_eps.size+1)), end = '\r')

            # increment the counter
            ind1 = ind1 + 1
        


    # print(eps_rpm_df)

    # now save the data
    # eps_rpm_df.to_csv('eps_rpm_df_LIZTEST_HOME-COMPY.csv', sep=',',)
    # alpha_rpm_df.to_csv('alpha_rpm_df_LIZTEST.csv', sep=',',)

    filename = 'eps_rpm_df_' + platform.node()+'_'+str(time.time())+'.csv'
    eps_rpm_df.to_csv(filename, sep=',',)

    return eps_rpm_df









def makeBigDataFrame_AlphaVsRPM(nominal_RPM_vec = np.linspace(30, 24000, 100),
                     # eps_ratio_vec = np.linspace(0.02, 0.65, 200),
                     alpha_percent_vec = np.linspace(0, 0.5, 100),
                     num_replicates = 100,
                     oversample_factor = 32, 
                     num_periods = 32,
                     duty = .05,
                     # alpha_percent = 0.1,
                     starting_seed = 48824):

    
    # define the nominal rpm and T vectors
    # nominal_RPM_vec = np.linspace(30, 24000, 200)  # rev/min
    T_vec = 60 / nominal_RPM_vec  # sec/rev

    xmin = 1

    # beta not used in this experiment
    beta_percent = 0
    max_pulse_height = 1 


    # get all the alpha_percent, T combinations
    X_alpha_percent, Y_T2 = np.meshgrid(alpha_percent_vec, T_vec)

    colNames4 = ['seed_no',
                 'eps_percent',
                 'alpha_percent',
                 'nominal_rpm',
                 'rpm_pers',
                 'rpm_fourier',
                 'time_pers',
                 'time_fourier']
    # initialize a Pandas data frame that will hold the output from the second for loop
    alpha_rpm_df = pd.DataFrame(columns=colNames4,
                            index=np.arange(num_replicates * X_alpha_percent.size),
                            dtype=np.float64)


    # define an index for the inner for loop. This will be used to increment the counter that stores the results of the
    # nested iteration
    ind1 = 0
    ind2 = 0

    seed = starting_seed

    # create a data frame for the iteration over epsilon vector, and rpm vector with num_replicates
    for rep in np.arange(num_replicates):
        # increment the seed for each replicate
        seed += 1

        # set needed parameters for loop 2
        # second loop is for the T, epsilon combinations
        # set a value for eps_percent where:
        # eps * T controls the amplitude of the uniform noise that will vary the distance between peak
        eps_percent = 0.25

        for alpha_percent, T in zip(np.ravel(X_alpha_percent), np.ravel(Y_T2)):


            # find the right bound of the time interval
            xmax = xmin + num_periods * T

            # find the number of samples
            N = int(np.ceil(1/T * max(oversample_factor, 2) * (xmax-xmin)))

            # get the nominal rpm
            nominalRPM = 1.0/T * 60


            # initialize the time vector
            t = np.linspace(xmin, xmax, N)


            # Return the random pulse
            out_noise = pulse_with_noise(t,T, duty, 
                                alpha_percent = alpha_percent, 
                                beta_percent =beta_percent, 
                                epsilon_percent=eps_percent, 
                                seed = seed, 
                                max_pulse_height = max_pulse_height)


            # force the beginning and end to be zero
            out_noise[0] = 0
            out_noise[-1] = 0

            # find the rpm using persistence
            start = time.perf_counter()
            rpm_pers = persistence_rpm(t, out_noise)
            end = time.perf_counter()
            pers_time = end - start

            # find rpm using fourier
            # find the index for the right bound of t
            t_right_ind = np.where(t >= (xmax - xmin))[0][0]

            # get the rpm using Fourier
            start = time.perf_counter()
            rpm_fourier = fourier_rpm(t, out_noise)
            end = time.perf_counter()
            fourier_time = end - start

            # print(pers_time - fourier_time)
            # if np.isnan(rpm_pers):
            #     print('yup')
            #     plt.plot(t,out)
            #     plt.show()
            # print(rep, eps_percent, T)
            # eps_rpm_df[rep, ] = ([rep, eps_percent, nominalRPM, rpm_pers, rpm_fourier, pers_time, fourier_time])
            # print(iter, rpm_fourier)
            # print(rpm_pers)

            # print("rep = {}, alpha_percent = {}, nominalRPM={}, eps_percent={}, rpm_pers={}, rpm_fourier={}"
            #     .format(rep, alpha_percent, nominalRPM, eps_percent, rpm_pers, rpm_fourier ))

            # print("rep+ind = {}".format(rep+ind))
            #
            alpha_rpm_df.iloc[ind2] = [int(rep), float(eps_percent), float(alpha_percent), float(nominalRPM),
                                    float(rpm_pers), float(rpm_fourier), float(pers_time), float(fourier_time)]

            # show progress
            # os.system('cls')
            print("Progress of loop 2 = {0:.2%}". format((rep + ind2)/(num_replicates * X_alpha_percent.size+1)), end = '\r')


            # increment the counter
            ind2 = ind2 + 1



    filename = 'alpha_rpm_df_' + platform.node()+'_'+str(time.time())+'.csv'
    alpha_rpm_df.to_csv(filename, sep=',',)

    return alpha_rpm_df

if __name__ == '__main__':
    # this part calculates the rpm from the experimental data using persistence and Fourier
    # specify the folder that contains the data
    folderToLoad = "C:/Users/firas/Documents/MATLAB/laser_tach_data_29Jun2017"
    # folderToLoad = "C:\\Users\\khasawn3\\Documents\\MATLAB\\laser_tach_data_29Jun2017"
    # get a list of files in that folder
    rpmFilesList = [f for f in listdir(folderToLoad) if isfile(join(folderToLoad, f))]

    # loop over the files in the list
    # preallocate the data frame
    colNames = ['trial_no', 'nominal_rpm', 'rpm_pers', 'rpm_fourier']
    out_df = pd.DataFrame(columns=colNames, index=rpmFilesList, dtype=np.float64)

    # start the loop
    for fileName in rpmFilesList:
        nominalRPM, trialNumber = get_nominal_rpm_and_trialno(fileName)
        x = hard_threshold(folderToLoad, fileName, 0, 1)

        # find the rpm using persistence
        rpm_pers = persistence_rpm(x[0], x[1])

        # find rpm using fourier
        rpm_fourier = fourier_rpm(x[0], x[1])

        # update the data frame
        out_df.loc[fileName] = [float(trialNumber), float(nominalRPM), float(rpm_pers), float(rpm_fourier)]

        # # redefine the index
        # out_df.index = np.arange(len(out_df.index))


        # get the stats
    df_stats = out_df.groupby(['nominal_rpm']).agg({'rpm_pers': ['mean', 'std'], 'rpm_fourier': ['mean', 'std']})

    print(df_stats)
    # print(out_df)
    # plot the results of the experiments
    out_df.rename(columns={"rpm_fourier": "Fourier", "rpm_pers": "Persistence"}, inplace=True)
    df_exp_plot = pd.melt(out_df, value_name="rpm", var_name="Method", id_vars=['nominal_rpm', 'trial_no'],
                              value_vars=['Persistence', 'Fourier'])
    fig, ax0 = plt.subplots()
    sns.tsplot(data=df_exp_plot, time="nominal_rpm", unit="trial_no", condition="Method", err_style="ci_band",
           value="rpm", estimator=np.nanmean, ax=ax0)

        # add inset figures
    axins1 = inset_locator.zoomed_inset_axes(ax0,
                                            zoom=3,
                                            loc=1,
                                            bbox_to_anchor=(0.8, 0.4),
                                            bbox_transform=ax0.transAxes,
                                            borderpad=0)  # zoom = 6

    sns.tsplot(data=df_exp_plot, time="nominal_rpm", unit="trial_no", condition="Method", err_style="ci_band",
           value="rpm", estimator=np.nanmean, ax=axins1)

    mark_inset(ax0, axins1, loc1a=3, loc1b=4, loc2a=2, loc2b=1, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 125, 225, 125, 225
    axins1.set_xlim(x1, x2)
    axins1.set_ylim(y1, y2)

    axins1.set_xlabel('')
    axins1.set_ylabel('')
    axins1.set_xticks([])
    axins1.set_yticks([])
    axins1.legend_.remove()

    axins2 = inset_locator.zoomed_inset_axes(ax0,
                                             zoom=3,
                                             loc=1,
                                             bbox_to_anchor=(0.7, 0.98),
                                             bbox_transform=ax0.transAxes,
                                             borderpad=0)  # zoom = 6

    sns.tsplot(data=df_exp_plot, time="nominal_rpm", unit="trial_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=axins2)

    mark_inset(ax0, axins2, loc1a=4, loc1b=3, loc2a=1, loc2b=2, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 930, 1030, 930, 1030
    axins2.set_xlim(x1, x2)
    axins2.set_ylim(y1, y2)

    axins2.set_xlabel('')
    axins2.set_ylabel('')
    axins2.set_xticks([])
    axins2.set_yticks([])
    axins2.legend_.remove()

    ax0.grid(color='gray', linewidth=1)
    ax0.set_xlabel('$\Omega_0$ (rpm)')
    ax0.set_ylabel('$\Omega_P$, $\Omega_F$ (rpm)')

    # show the figure
    # plt.tight_layout(w_pad=0.5, h_pad=0.5)
    plt.tight_layout()
    plt.savefig('experimental_rpm_comparison.pdf')
    plt.show()


    # # specify the number of periods to capture (using nominal period)
    # num_periods = 32
    #
    # # T = 0.01/4  # nominal period in seconds/rev
    # duty = .05  # fraction of the period the pulse is on
    # tau = duty * T
    #
    # # noise to add in the x-direction (causes digital noise)
    # # noise as a fraction of the +1 pulse
    # alpha_percent = 0.1
    # alpha = tau * alpha_percent
    #
    # beta_percent = 0
    # max_pulse_height = 1
    # beta = max_pulse_height * beta_percent  # noise to add in the y-direction
    #
    # xmin = 1  # left bound of the time interval
    # xmax = xmin + num_periods * T  # find the right bound of the time interval
    # print("xmin={0}, xmax={1}".format(xmin, xmax))

    # # define a vector that will hold the ratio of noise amplitude to +1 pulse width
    # eps_ratio_vec = np.linspace(0.02, 0.65, 200)


    # N = int(np.ceil(1/T * max(oversample_factor, 2) * (xmax-xmin)))
    # # N = 10000
    #
    # # get the nominal rpm
    # nominalRPM = 1.0/T * 60
    #
    # # print("N={0}, nominalRPM={1}".format(N, nominalRPM))
    #
    # # initialize the time vector
    # t = np.linspace(xmin, xmax, N)

    # # print(t.shape)
    #
    # # begin the loop over the ratio vector
    # # preallocate the data frame
    # colNames2 = ['trial_no', 'nominal_rpm', 'rpm_pers', 'rpm_fourier']
    # out_df2 = pd.DataFrame(columns=colNames2, index=np.arange(len(eps_ratio_vec)), dtype=np.float64)
    #
    #
    # # start a for loop for the eps-percent vector
    # for iternum in np.arange(0, len(eps_ratio_vec)):
    #     eps_percent = eps_ratio_vec[iternum]
    #     #------------create a pulse with varying shape (width of T varies over time)
    #     # initialize Q
    #     Q = []
    #
    #     while sum(Q) < xmax:
    #         # get the list of random period lengths drawn from a uniform distribution
    #         # of epsilon/T vector
    #         # epsilon=iter*T is the amplitude of the uniform noise
    #         eps = eps_percent * T  # epsilon
    #         Q.append(T + uni(10, eps)[0])
    #
    #     # this find the ocations of the beginning of each period
    #     C = [sum(Q[:i]) for i in range(len(Q)+1)]
    #
    #     # this tages each entry in the time vector by which period it belongs to
    #     index = [np.where(np.array(C)<x)[0][-1] for x in list(t)]
    #
    #     # phi respaces the real line to match the period
    #     phi = [T*i+(T/Q[i])*(t[j] - C[i]) for j,i in enumerate(index)]
    #     phi = np.array(phi)
    #
    #     # now find the noise-free pulse with varying shape
    #     out = pulse(phi, T, tau) * max_pulse_height
    #     #------------create a pulse with varying shape (width of T varies over time)
    #
    # # # plt.plot(t,phi)
    # # # plt.plot([0,15],[0,15])
    # # # plt.scatter(C,[T*i for i in range(len(C))])
    # #
    # # print('Avg Q_i is', np.average(Q))
    #
    #     # this adds the digital noise (fake pulses at rising/falling edge)
    #     noise = uni(N, alpha / 2, seed=48824)
    #     phi_noise = phi + noise
    #
    #
    #     out_noise = pulse(phi_noise, T,tau) * max_pulse_height
    #
    #     # force the beginning and end to be zero
    #     out_noise[0] = 0
    #     out_noise[-1] = 0
    #
    #     # find the rpm using persistence
    #     start = time.process_time()
    #     rpm_pers = persistence_rpm(t, out_noise)
    #     end = time.process_time()
    #     time_elapsed = end - start
    #
    #     # find rpm using fourier
    #     # find the index for the right bound of t
    #     t_right_ind = np.where(t >= (xmax - xmin))[0][0]
    #
    #
    #     # get the rpm using Fourier
    #     start = time.process_time()
    #     rpm_fourier = fourier_rpm(t, out_noise)
    #     end = time.process_time()
    #     time_elapsed = end - start
    #
    #     # if np.isnan(rpm_pers):
    #     #     print('yup')
    #     #     plt.plot(t,out)
    #     #     plt.show()
    #
    #
    #
    #     # print(iter, rpm_fourier)
    #     # print(rpm_pers)
    #
    #     out_df2.loc[iternum] = [float(eps_percent), float(nominalRPM),
    #                             float(rpm_pers), float(rpm_fourier)]
    # # print(out_df2)
    #
    # # get the stats
    # df_stats2= out_df2.groupby(['nominal_rpm'])\
    #         .agg({'rpm_pers': ['mean', 'std'], 'rpm_fourier': ['mean', 'std']})
    #
    # print(df_stats2)

    # # plot the results
    # plt.plot(out_df2.loc[:, 'trial_no'], out_df2.loc[:, 'nominal_rpm'], 'k',
    #          out_df2.loc[:, 'trial_no'], out_df2.loc[:, 'rpm_pers'], 'b',
    #          out_df2.loc[:, 'trial_no'], out_df2.loc[:, 'rpm_fourier'], 'g')
    # # print(out_df2.loc[:, 'rpm_pers'])
    # plt.xlabel(r"$\frac{\epsilon}{T}$", fontsize=16)
    # plt.ylabel('RPM')
    # plt.show()
    #
    # # plt.plot(t,out_noise)
    # plt.plot(t, out)
    # plt.scatter(t, out_noise)
    # plt.show()


    # np.savetxt('exampleLaserTachVariable_t.txt', t)
    # np.savetxt('exampleLaserTachVariable_y.txt',out_noise)


    # # paper, long calculations. This generates synthetic data.
    # # define the nominal rpm and T vectors
    # nominal_RPM_vec = np.linspace(30, 24000, 200)  # rev/min
    # T_vec = 60 / nominal_RPM_vec  # sec/rev
    #
    # # define a vector that will hold the ratio of noise amplitude to +1 pulse width
    # eps_ratio_vec = np.linspace(0.02, 0.65, 200)
    #
    # # get all the combinations using meshgrid
    # X_eps, Y_T = np.meshgrid(eps_ratio_vec, T_vec)
    #
    # # number of replicates
    # num_replicates = 1000
    #
    # # get a starting seed
    # starting_seed = 48824
    #
    # # initialize a Pandas data frame that will hold the output from the first for loop
    # colNames3 = ['seed_no', 'alpha_percent', 'eps_over_T', 'nominal_rpm', 'rpm_pers', 'rpm_fourier',
    #              'time_pers', 'time_fourier']
    # eps_rpm_df = pd.DataFrame(columns=colNames3,
    #                           index=np.arange(num_replicates * X_eps.size+1), dtype=np.float64)
    #
    # # define a vector for alpha percent
    # alpha_percent_vec = np.linspace(0, 0.5, 200)
    #
    # # get all the alpha_percent, T combinations
    # X_alpha_percent, Y_T2 = np.meshgrid(alpha_percent_vec, T_vec)
    #
    # # initialize a Pandas data frame that will hold the output from the second for loop
    # colNames4 = ['seed_no', 'alpha_percent', 'eps_percent', 'nominal_rpm', 'rpm_pers', 'rpm_fourier',
    #              'time_pers', 'time_fourier']
    # alpha_rpm_df = pd.DataFrame(columns=colNames4,
    #                           index=np.arange(num_replicates * X_alpha_percent.size+1), dtype=np.float64)
    #
    # # print("shape of eps_rpm_df = {}".format(eps_rpm_df.shape))
    #
    # # noise level in the y-direction (unused in this paper)
    # beta_percent = 0
    # max_pulse_height = 1
    #
    # # calculate the needed number of samples
    # oversample_factor = 32  # oversample by this rate. Must be at least 2
    #
    # xmin = 1  # left bound of the time interval
    #
    # # specify the number of periods to capture (using nominal period)
    # num_periods = 32
    #
    # # T = 0.01/4  # nominal period in seconds/rev
    # duty = .05  # fraction of the period the pulse is on
    #
    # # define an index for the inner for loop. This will be used to increment the counter that stores the results of the
    # # nested iteration
    # ind1 = 0
    # ind2 = 0
    #
    # # create a data frame for the iteration over epsilon vector, and rpm vector with num_replicates
    # for rep in np.arange(num_replicates):
    #     # increment the seed for each replicate
    #     # print(rep)
    #     newseed = starting_seed + 1
    #
    #     # set needed parameters for loop 1
    #     # noise as a fraction of the +1 pulse
    #     alpha_percent = 0.1
    #
    #     # first loop is for the T, epsilon combinations
    #     for eps_percent, T in zip(np.ravel(X_eps), np.ravel(Y_T)):
    #         # find the width of the spike
    #         tau = duty * T
    #
    #         # print progress
    #         # print("(T, epsilon)=({0}, {1})".format(T, eps_percent))
    #
    #         # noise to add in the x-direction (causes digital noise)
    #         alpha = tau * alpha_percent
    #
    #         # noise to add in the y-direction
    #         beta = max_pulse_height * beta_percent
    #         # find the right bound of the time interval
    #         xmax = xmin + num_periods * T
    #
    #         # find the number of samples
    #         N = int(np.ceil(1/T * max(oversample_factor, 2) * (xmax-xmin)))
    #         # N = 10000
    #
    #         # get the nominal rpm
    #         nominalRPM = 1.0/T * 60
    #
    #         # print("N={0}, nominalRPM={1}".format(N, nominalRPM))
    #
    #         # initialize the time vector
    #         t = np.linspace(xmin, xmax, N)
    #         # ------------create a pulse with varying shape (width of T varies over time)
    #         # initialize Q
    #         Q = []
    #
    #         while sum(Q) < xmax:
    #             # get the list of random period lengths drawn from a uniform distribution
    #             # of epsilon/T vector
    #             # epsilon=iter*T is the amplitude of the uniform noise
    #             eps = eps_percent * T  # epsilon
    #             Q.append(T + uni(10, eps)[0])
    #
    #         # this find the locations of the beginning of each period
    #         C = [sum(Q[:i]) for i in range(len(Q)+1)]
    #
    #         # this tages each entry in the time vector by which period it belongs to
    #         index = [np.where(np.array(C)<x)[0][-1] for x in list(t)]
    #
    #         # phi respaces the real line to match the period
    #         phi = [T*i+(T/Q[i])*(t[j] - C[i]) for j,i in enumerate(index)]
    #         phi = np.array(phi)
    #
    #         # now find the noise-free pulse with varying shape
    #         out = pulse(phi, T, tau) * max_pulse_height
    #         # ------------create a pulse with varying shape (width of T varies over time)
    #
    #     # # plt.plot(t,phi)
    #     # # plt.plot([0,15],[0,15])
    #     # # plt.scatter(C,[T*i for i in range(len(C))])
    #     #
    #     # print('Avg Q_i is', np.average(Q))
    #
    #         # this adds the digital noise (fake pulses at rising/falling edge)
    #         noise = uni(N, alpha / 2, seed=newseed)
    #         phi_noise = phi + noise
    #
    #
    #         out_noise = pulse(phi_noise, T,tau) * max_pulse_height
    #
    #         # force the beginning and end to be zero
    #         out_noise[0] = 0
    #         out_noise[-1] = 0
    #
    #         # find the rpm using persistence
    #         start = time.perf_counter()
    #         rpm_pers = persistence_rpm(t, out_noise)
    #         end = time.perf_counter()
    #         pers_time = end - start
    #
    #         # find rpm using fourier
    #         # find the index for the right bound of t
    #         t_right_ind = np.where(t >= (xmax - xmin))[0][0]
    #
    #         # get the rpm using Fourier
    #         start = time.perf_counter()
    #         rpm_fourier = fourier_rpm(t, out_noise)
    #         end = time.perf_counter()
    #         fourier_time = end - start
    #
    #         # print(pers_time - fourier_time)
    #         # if np.isnan(rpm_pers):
    #         #     print('yup')
    #         #     plt.plot(t,out)
    #         #     plt.show()
    #         # print(rep, eps_percent, T)
    #         # eps_rpm_df[rep, ] = ([rep, eps_percent, nominalRPM, rpm_pers, rpm_fourier, pers_time, fourier_time])
    #         # print(iter, rpm_fourier)
    #         # print(rpm_pers)
    #         #
    #         eps_rpm_df.iloc[rep + ind1] = [int(rep), float(alpha_percent), float(eps_percent), float(nominalRPM),
    #                                 float(rpm_pers), float(rpm_fourier), float(pers_time), float(fourier_time)]
    #         # increment the counter
    #         ind1 = ind1 + 1
    #
    #         # show progress
    #         # os.system('cls')
    #         print("Progress of loop 1 = {0:.2%}". format((rep + ind1)/(num_replicates * X_eps.size+1)))
    #
    #     # set needed parameters for loop 2
    #     # second loop is for the T, epsilon combinations
    #     # set a value for eps_percent where:
    #     # eps * T controls the amplitude of the uniform noise that will vary the distance between peak
    #     eps_percent = 0.25
    #
    #     for alpha_percent, T in zip(np.ravel(X_alpha_percent), np.ravel(Y_T2)):
    #         # find the width of the spike
    #         tau = duty * T
    #
    #         # print progress
    #         # print("(T, epsilon)=({0}, {1})".format(T, eps_percent))
    #
    #         # noise to add in the x-direction (causes digital noise)
    #         alpha = tau * alpha_percent
    #
    #         # noise to add in the y-direction
    #         beta = max_pulse_height * beta_percent
    #         # find the right bound of the time interval
    #         xmax = xmin + num_periods * T
    #
    #         # find the number of samples
    #         N = int(np.ceil(1/T * max(oversample_factor, 2) * (xmax-xmin)))
    #         # N = 10000
    #
    #         # get the nominal rpm
    #         nominalRPM = 1.0/T * 60
    #
    #         # print("N={0}, nominalRPM={1}".format(N, nominalRPM))
    #
    #         # initialize the time vector
    #         t = np.linspace(xmin, xmax, N)
    #         # ------------create a pulse with varying shape (width of T varies over time)
    #         # initialize Q
    #         Q = []
    #
    #         while sum(Q) < xmax:
    #             # get the list of random period lengths drawn from a uniform distribution
    #             # of epsilon/T vector
    #             # epsilon=eps_percent * T is the amplitude of the uniform noise
    #             eps = eps_percent * T  # epsilon
    #             Q.append(T + uni(10, eps)[0])
    #
    #         # this find the locations of the beginning of each period
    #         C = [sum(Q[:i]) for i in range(len(Q)+1)]
    #
    #         # this tages each entry in the time vector by which period it belongs to
    #         index = [np.where(np.array(C)<x)[0][-1] for x in list(t)]
    #
    #         # phi respaces the real line to match the period
    #         phi = [T*i+(T/Q[i])*(t[j] - C[i]) for j,i in enumerate(index)]
    #         phi = np.array(phi)
    #
    #         # now find the noise-free pulse with varying shape
    #         out = pulse(phi, T, tau) * max_pulse_height
    #         # ------------create a pulse with varying shape (width of T varies over time)
    #
    #     # # plt.plot(t,phi)
    #     # # plt.plot([0,15],[0,15])
    #     # # plt.scatter(C,[T*i for i in range(len(C))])
    #     #
    #     # print('Avg Q_i is', np.average(Q))
    #
    #         # this adds the digital noise (fake pulses at rising/falling edge)
    #         noise = uni(N, alpha / 2, seed=newseed)
    #         phi_noise = phi + noise
    #
    #
    #         out_noise = pulse(phi_noise, T,tau) * max_pulse_height
    #
    #         # force the beginning and end to be zero
    #         out_noise[0] = 0
    #         out_noise[-1] = 0
    #
    #         # find the rpm using persistence
    #         start = time.perf_counter()
    #         rpm_pers = persistence_rpm(t, out_noise)
    #         end = time.perf_counter()
    #         pers_time = end - start
    #
    #         # find rpm using fourier
    #         # find the index for the right bound of t
    #         t_right_ind = np.where(t >= (xmax - xmin))[0][0]
    #
    #         # get the rpm using Fourier
    #         start = time.perf_counter()
    #         rpm_fourier = fourier_rpm(t, out_noise)
    #         end = time.perf_counter()
    #         fourier_time = end - start
    #
    #         # print(pers_time - fourier_time)
    #         # if np.isnan(rpm_pers):
    #         #     print('yup')
    #         #     plt.plot(t,out)
    #         #     plt.show()
    #         # print(rep, eps_percent, T)
    #         # eps_rpm_df[rep, ] = ([rep, eps_percent, nominalRPM, rpm_pers, rpm_fourier, pers_time, fourier_time])
    #         # print(iter, rpm_fourier)
    #         # print(rpm_pers)
    #
    #         # print("rep = {}, alpha_percent = {}, nominalRPM={}, eps_percent={}, rpm_pers={}, rpm_fourier={}"
    #         #     .format(rep, alpha_percent, nominalRPM, eps_percent, rpm_pers, rpm_fourier ))
    #
    #         # print("rep+ind = {}".format(rep+ind))
    #         #
    #         alpha_rpm_df.iloc[rep + ind2] = [int(rep), float(eps_percent), float(alpha_percent), float(nominalRPM),
    #                                 float(rpm_pers), float(rpm_fourier), float(pers_time), float(fourier_time)]
    #         # increment the counter
    #         ind2 = ind2 + 1
    #
    #         # show progress
    #         # os.system('cls')
    #         print("Progress of loop 2 = {0:.2%}". format((rep + ind2)/(num_replicates * X_alpha_percent.size+1)))
    #
    #
    # # print(eps_rpm_df)
    #
    # # now save the data
    # eps_rpm_df.to_csv('eps_rpm_df.csv', sep=',',)
    # alpha_rpm_df.to_csv('alpha_rpm_df.csv', sep=',',)
