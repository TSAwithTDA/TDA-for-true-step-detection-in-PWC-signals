# this file plots the necessary pictures for the laser tach paper

import os.path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import ttest_1samp, ttest_rel, normaltest
from mpl_toolkits.axes_grid import inset_locator
from mpl_toolkits.axes_grid1.inset_locator import TransformedBbox, BboxPatch, BboxConnector
import matplotlib
matplotlib.style.use('ggplot')

# from matplotlib.colors import LogNorm
import seaborn as sns
# sns.set(font_scale=1.5)  # switch to seaborn defaults
sns.set_style("ticks")

# setting plotting style to latex
plt.rc('text', usetex=True)
plt.rc('font', family='serif')


# load the data file
data_folder = "C:/Users/firas/Dropbox/WorkStuff/Research/Jour_ART/lasertach_git/Code/wavePulseProcessingWithTDA"
# data_folder = "C:/Users/khasawn3/Dropbox/WorkStuff\Research/Jour_ART/lasertach_git/Code/wavePulseProcessingWithTDA"
alpha_data_file = "alpha_rpm_df_optiplex-7050_1507228448.801099.csv"
epsilon_data_file = "eps_rpm_df_optiplex-7050_1507228534.8371167.csv"

# read the alpha data into x_alpha, and the epsilon data into x_epsilon
x_alpha = pd.read_csv(os.path.join(data_folder, alpha_data_file), index_col=0)
x_epsilon = pd.read_csv(os.path.join(data_folder, epsilon_data_file), index_col=0)

# find the means
y_alpha_pers = x_alpha.groupby(['alpha_percent',
               'nominal_rpm'])['rpm_pers'].mean().reset_index(name='rpm_pers')
y_alpha_fourier = x_alpha.groupby(['alpha_percent',
               'nominal_rpm'])['rpm_fourier'].mean().reset_index(name='rpm_fourier')
#
y_epsilon_pers = x_epsilon.groupby(['eps_over_T',
               'nominal_rpm'])['rpm_pers'].mean().reset_index(name='rpm_pers')
y_epsilon_fourier = x_epsilon.groupby(['eps_over_T',
               'nominal_rpm'])['rpm_fourier'].mean().reset_index(name='rpm_fourier')

# calculate the differences from nominal rpm
y_alpha_pers['rpm_pers'] = \
    (np.abs(y_alpha_pers['rpm_pers'] - y_alpha_pers['nominal_rpm']))/ y_alpha_pers['nominal_rpm']
y_epsilon_pers['rpm_pers'] = \
    (np.abs(y_epsilon_pers['rpm_pers'] - y_epsilon_pers['nominal_rpm']))/y_epsilon_pers['nominal_rpm']
#
y_alpha_fourier['rpm_fourier'] = \
    (np.abs(y_alpha_fourier['rpm_fourier'] - y_alpha_fourier['nominal_rpm']))/y_alpha_fourier['nominal_rpm']
y_epsilon_fourier['rpm_fourier'] = \
    (np.abs(y_epsilon_fourier['rpm_fourier'] - y_epsilon_fourier['nominal_rpm']))/y_epsilon_fourier['nominal_rpm']

# pivot the table
df_alpha_pers = y_alpha_pers.pivot(index='alpha_percent', columns='nominal_rpm', values='rpm_pers')
df_alpha_fourier = y_alpha_fourier.pivot(index='alpha_percent', columns='nominal_rpm', values='rpm_fourier')
#
df_epsilon_pers = y_epsilon_pers.pivot(index='eps_over_T', columns='nominal_rpm', values='rpm_pers')
df_epsilon_fourier = y_epsilon_fourier.pivot(index='eps_over_T', columns='nominal_rpm', values='rpm_fourier')

# find the indices for three vertical lines that correspond to three selected rpms
rpm1 = 3000
rpm2 = 12000
rpm3 = 20000
ind1 = np.where(y_alpha_pers['nominal_rpm'] >= rpm1)
ind2 = np.where(y_alpha_pers['nominal_rpm'] >= rpm2)
ind3 = np.where(y_alpha_pers['nominal_rpm'] >= rpm3)


#
# # define the color map
cmap = "magma_r"
bmap = "BuPu"
bmap = 'rainbow'
#
# # specify the number of tick points
num_xticks = 12
num_yticks = 12


# get figure 1 (all heatmaps figure)
def plot_all_heat_maps():
    # now plot epsilon versus nominal rpm
    # get a handle for the axes
    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)

    # find the min and max values of the color bar
    vmax_row1 = np.max([y_epsilon_fourier['rpm_fourier'].max(), y_epsilon_pers['rpm_pers'].max()])
    vmax_row2 = np.max([y_alpha_fourier['rpm_fourier'].max(), y_alpha_pers['rpm_pers'].max()])
    #
    # vmax_row1 = 0.3
    # vmax_row2 = 0.3
    # get the heatmap
    sns.heatmap(df_epsilon_pers, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                cbar=False, vmin=0, vmax=vmax_row1, ax=ax0)
    sns.heatmap(df_epsilon_fourier, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                vmin=0, vmax=vmax_row1, ax=ax1)
    #
    sns.heatmap(df_alpha_pers, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                cbar=False,  vmin=0, vmax=vmax_row2, ax=ax2)
    sns.heatmap(df_alpha_fourier, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                vmin=0, vmax=vmax_row2, ax=ax3)

    # # plot three vertical lines to mark the values of the nominal rpm for the other 3 plots
    # ax0.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax0.get_ylim(),  linestyle='dashed', linewidth=1)
    # ax1.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax1.get_ylim(),  linestyle='dashed', linewidth=1)
    # ax2.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax2.get_ylim(),  linestyle='dashed', linewidth=1)
    # ax3.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax3.get_ylim(),  linestyle='dashed', linewidth=1)

    # reverse the y-axis so its values are increasing vertically up
    ax0.invert_yaxis()
    # ax1.invert_yaxis()
    # ax2.invert_yaxis()
    # ax3.invert_yaxis()

    # get the xticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_xticklabels()]
    texts_ax1 = [t.get_text() for t in ax1.get_xticklabels()]
    texts_ax2 = [t.get_text() for t in ax2.get_xticklabels()]
    texts_ax3 = [t.get_text() for t in ax3.get_xticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)
    zz_ax1 = np.asarray(texts_ax1, dtype=float)
    zz_ax2 = np.asarray(texts_ax2, dtype=float)
    zz_ax3 = np.asarray(texts_ax3, dtype=float)

    # format the xticks
    xticks_ax0 = ['{:0.0f}'.format(x) for x in zz_ax0]
    xticks_ax1 = ['{:0.0f}'.format(x) for x in zz_ax1]
    xticks_ax2 = ['{:0.0f}'.format(x) for x in zz_ax2]
    xticks_ax3 = ['{:0.0f}'.format(x) for x in zz_ax3]

    # get the yticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_yticklabels()]
    texts_ax1 = [t.get_text() for t in ax1.get_yticklabels()]
    texts_ax2 = [t.get_text() for t in ax2.get_yticklabels()]
    texts_ax3 = [t.get_text() for t in ax3.get_yticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)
    zz_ax1 = np.asarray(texts_ax1, dtype=float)
    zz_ax2 = np.asarray(texts_ax2, dtype=float)
    zz_ax3 = np.asarray(texts_ax3, dtype=float)

    # format the yticks
    yticks_ax0 = ['{:0.2f}'.format(x) for x in zz_ax0]
    yticks_ax1 = ['{:0.2f}'.format(x) for x in zz_ax1]
    yticks_ax2 = ['{:0.2f}'.format(x) for x in zz_ax2]
    yticks_ax3 = ['{:0.2f}'.format(x) for x in zz_ax3]

    # format the axes
    xtick_rot = 45
    ax0.set_xlabel('')
    ax0.set_ylabel('$\\epsilon$')
    ax0.set_xticklabels(labels=xticks_ax0, rotation=xtick_rot)
    ax0.set_yticklabels(labels=yticks_ax0, rotation=0)
    #
    ax1.set_xlabel('')
    ax1.set_ylabel('')
    ax1.set_xticklabels(labels=xticks_ax1, rotation=xtick_rot)
    ax1.set_yticklabels(labels=yticks_ax1, rotation=0)
    #
    ax2.set_xlabel('$\\Omega_0$')
    ax2.set_ylabel('$\\alpha$')
    ax2.set_xticklabels(labels=xticks_ax2, rotation=xtick_rot)
    ax2.set_yticklabels(labels=yticks_ax2, rotation=0)
    #
    ax3.set_xlabel('$\\Omega_0$')
    ax3.set_ylabel('')
    ax3.set_xticklabels(labels=xticks_ax3, rotation=xtick_rot)
    ax3.set_yticklabels(labels=yticks_ax3, rotation=0)

    # show the figure
    plt.tight_layout(w_pad=0.5, h_pad=0.5)
    plt.savefig('full_heat_map_comparison.png', dpi=200)
    # plt.show()

# plot_all_heat_maps()


# draw a bbox of the region of the inset axes in the parent axes and
# connecting lines between the bbox and the inset axes area
# loc1, loc2 : {1, 2, 3, 4}
def mark_inset(parent_axes, inset_axes, loc1a=1, loc1b=1, loc2a=2, loc2b=2, **kwargs):
    rect = TransformedBbox(inset_axes.viewLim, parent_axes.transData)

    pp = BboxPatch(rect, fill=False, **kwargs)
    parent_axes.add_patch(pp)

    p1 = BboxConnector(inset_axes.bbox, rect, loc1=loc1a, loc2=loc1b, **kwargs)
    inset_axes.add_patch(p1)
    p1.set_clip_on(False)
    p2 = BboxConnector(inset_axes.bbox, rect, loc1=loc2a, loc2=loc2b, **kwargs)
    inset_axes.add_patch(p2)
    p2.set_clip_on(False)

    return pp, p1, p2


def plot_epsilon_figs():
    # figure 2 - epsilon plots which includes a 2 x 2 plots where:
    # plot 1 is the heatmap for rpm_persistence versus nominal rpm and eps_percent
    # plot 2 is the plot for chosen nominal rpm1 versus rpm_pers (nominal_rpm is overlaid as a horizontal line)
    # plot 3 is the plot for chosen nominal rpm1 versus rpm_pers (nominal_rpm is overlaid as a horizontal line)
    # plot 4 is the plot for chosen nominal rpm1 versus rpm_pers (nominal_rpm is overlaid as a horizontal line)

    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2)

    # plot subfigure 1 (the heat map)
    sns.heatmap(df_epsilon_pers, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                cbar=True, ax=ax0)

    # get the 'true' rpms
    # find the indices for three vertical lines that correspond to three selected rpms
    rpm1 = 3000
    rpm2 = 12000
    rpm3 = 20000
    ind1 = np.where(x_epsilon['nominal_rpm'] >= rpm1)
    ind2 = np.where(x_epsilon['nominal_rpm'] >= rpm2)
    ind3 = np.where(x_epsilon['nominal_rpm'] >= rpm3)

    # plot #2
    # find the data frames that contain the information for the other plots
    true_rpm1 = x_epsilon.iloc[ind1[0][0]]['nominal_rpm']
    true_rpm2 = x_epsilon.iloc[ind2[0][0]]['nominal_rpm']
    true_rpm3 = x_epsilon.iloc[ind3[0][0]]['nominal_rpm']

    # melt the data
    # first rename the columns in preparation for later plotting
    x_epsilon.rename(columns={"rpm_fourier": "Fourier", "rpm_pers": "Persistence"}, inplace=True)

    # print(x_epsilon.head())
    # print(true_rpm1, true_rpm2, true_rpm3)

    # now melt the data
    df_plot2_melted = pd.melt(x_epsilon, value_name="rpm", var_name="Method", id_vars=['nominal_rpm', 'eps_over_T', 'seed_no'],
                              value_vars=['Persistence', 'Fourier'])

    df_plot2_rpm1 = df_plot2_melted.query('nominal_rpm == @true_rpm1')
    df_plot2_rpm2 = df_plot2_melted.query('nominal_rpm == @true_rpm2')
    df_plot2_rpm3 = df_plot2_melted.query('nominal_rpm == @true_rpm3')
    # .groupby('eps_over_T')['rpm_pers', 'rpm_fourier', 'time_pers', 'time_fourier']


    # plot three vertical lines to mark the values of the nominal rpm for the other 3 plots
    ax0.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax0.get_ylim(),  linestyle='dashed', linewidth=1)

    # reverse the y-axis so its values are increasing vertically up
    ax0.invert_yaxis()

    # get the xticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_xticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)

    # format the xticks
    xticks_ax0 = ['{:0.0f}'.format(x) for x in zz_ax0]

    # get the yticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_yticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)

    # format the yticks
    yticks_ax0 = ['{:0.2f}'.format(x) for x in zz_ax0]

    # format the axes
    xtick_rot = 45
    ax0.set_xlabel('$\\Omega_0$')
    ax0.set_ylabel('$\\epsilon$')
    ax0.set_xticklabels(labels=xticks_ax0, rotation=xtick_rot)
    ax0.set_yticklabels(labels=yticks_ax0, rotation=0)

    # plot subfigure 2
    sns.tsplot(data=df_plot2_rpm1, time="eps_over_T", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax1)
    ax1.hlines(true_rpm1, *ax1.get_xlim(),  linewidth=1)
    ax1.set_xlabel('$\\epsilon$')
    ax1.set_ylabel('$\Omega_P$, $\Omega_F$ (rpm)')

    # plot subfigure 3
    sns.tsplot(data=df_plot2_rpm2, time="eps_over_T", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax2)
    ax2.hlines(true_rpm2, *ax2.get_xlim(),  linewidth=1)
    ax2.set_xlabel('$\\epsilon$')
    ax2.set_ylabel('$\Omega_P$, $\Omega_F$ (rpm)')

    # plot subfigure 4
    sns.tsplot(data=df_plot2_rpm3, time="eps_over_T", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax3)
    ax3.hlines(true_rpm3, *ax3.get_xlim(),  linewidth=1)
    ax3.set_xlabel('$\\epsilon$')
    ax3.set_ylabel('$\Omega_P$, $\Omega_F$ (rpm)')

    # add inset figures
    axins1 = inset_locator.zoomed_inset_axes(ax1,
                                            zoom=1.8,
                                            loc=1,
                                            bbox_to_anchor=(0.85, 0.8),
                                            bbox_transform=ax1.transAxes,
                                            borderpad=0)  # zoom = 6

    sns.tsplot(data=df_plot2_rpm1, time="eps_over_T", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=axins1)

    mark_inset(ax1, axins1, loc1a=4, loc1b=4, loc2a=2, loc2b=2, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 0.55, 0.65, 2980, 3180
    axins1.set_xlim(x1, x2)
    axins1.set_ylim(y1, y2)

    axins1.set_xlabel('')
    axins1.set_ylabel('')
    axins1.set_xticks([])
    axins1.set_yticks([])
    axins1.legend_.remove()

    # add another inset figure
    axins2 = inset_locator.zoomed_inset_axes(ax1,
                                            zoom=1.8,
                                            loc=4,
                                            bbox_to_anchor=(0.31, 0.5),
                                            bbox_transform=ax1.transAxes,
                                            borderpad=0)  # zoom = 6

    sns.tsplot(data=df_plot2_rpm1, time="eps_over_T", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=axins2)

    mark_inset(ax1, axins2, loc1a=1, loc1b=2, loc2a=3, loc2b=3, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 0.32, 0.42, 1920, 2120
    axins2.set_xlim(x1, x2)
    axins2.set_ylim(y1, y2)

    axins2.set_xlabel('')
    axins2.set_ylabel('')
    axins2.set_xticks([])
    axins2.set_yticks([])
    axins2.legend_.remove()

    # # show the figure
    plt.tight_layout(w_pad=0.5, h_pad=0.5)
    plt.savefig('pers_vary_eps.png', dpi=200)
    plt.show()


def plot_alpha_figs():
    # figure 3, alpha plots
    fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2)

    # plot subfigure 1 (the heat map)
    sns.heatmap(df_alpha_pers, cmap=bmap, xticklabels=num_xticks, yticklabels=num_yticks,
                cbar=True, ax=ax0)

    # get the 'true' rpms
    # find the indices for three vertical lines that correspond to three selected rpms
    rpm1 = 3000
    rpm2 = 12000
    rpm3 = 20000
    ind1 = np.where(x_alpha['nominal_rpm'] >= rpm1)
    ind2 = np.where(x_alpha['nominal_rpm'] >= rpm2)
    ind3 = np.where(x_alpha['nominal_rpm'] >= rpm3)

    # find the data frames that contain the information for the other plots
    true_rpm1 = x_alpha.iloc[ind1[0][0]]['nominal_rpm']
    true_rpm2 = x_alpha.iloc[ind2[0][0]]['nominal_rpm']
    true_rpm3 = x_alpha.iloc[ind3[0][0]]['nominal_rpm']

    # melt the data
    # first rename the columns in preparation for later plotting
    x_alpha.rename(columns={"rpm_fourier": "Fourier", "rpm_pers": "Persistence"}, inplace=True)

    # print(x_alpha.head())

    # now melt the data
    df_plot3_melted = pd.melt(x_alpha, value_name="rpm", var_name="Method", id_vars=['nominal_rpm', 'alpha_percent', 'seed_no'],
                              value_vars=['Persistence', 'Fourier'])

    df_plot3_rpm1 = df_plot3_melted.query('nominal_rpm == @true_rpm1')
    df_plot3_rpm2 = df_plot3_melted.query('nominal_rpm == @true_rpm2')
    df_plot3_rpm3 = df_plot3_melted.query('nominal_rpm == @true_rpm3')
    # .groupby('eps_over_T')['rpm_pers', 'rpm_fourier', 'time_pers', 'time_fourier']

    # to do: add the figures of rpm_pers versus epsilon for the three values of rpm selected.
    #
    #

    # plot three vertical lines to mark the values of the nominal rpm for the other 3 plots
    ax0.vlines([ind1[0][0], ind2[0][0], ind3[0][0]], *ax0.get_ylim(),  linestyle='dashed', linewidth=1)

    # reverse the y-axis so its values are increasing vertically up
    ax0.invert_yaxis()

    # get the xticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_xticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)

    # format the xticks
    xticks_ax0 = ['{:0.0f}'.format(x) for x in zz_ax0]

    # get the yticks labels
    texts_ax0 = [t.get_text() for t in ax0.get_yticklabels()]

    # covert the obtained xticks to floats
    zz_ax0 = np.asarray(texts_ax0, dtype=float)

    # format the yticks
    yticks_ax0 = ['{:0.2f}'.format(x) for x in zz_ax0]

    # format the axes
    xtick_rot = 45
    ax0.set_xlabel('$\\Omega_0$')
    ax0.set_ylabel('$\\alpha$')
    ax0.set_xticklabels(labels=xticks_ax0, rotation=xtick_rot)
    ax0.set_yticklabels(labels=yticks_ax0, rotation=0)

    # plot subfigure 2
    sns.tsplot(data=df_plot3_rpm1, time="alpha_percent", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax1)
    ax1.hlines(true_rpm1, *ax1.get_xlim(),  linewidth=1)
    ax1.set_xlabel('$\\alpha$')
    ax1.set_ylabel('$\\Omega_P$, $\\Omega_F$ (rpm)')

    # plot subfigure 3
    sns.tsplot(data=df_plot3_rpm2, time="alpha_percent", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax2)
    ax2.hlines(true_rpm2, *ax2.get_xlim(),  linewidth=1)
    ax2.set_xlabel('$\\alpha$')
    ax2.set_ylabel('$\\Omega_P$, $\\Omega_F$ (rpm)')

    # plot subfigure 4
    sns.tsplot(data=df_plot3_rpm3, time="alpha_percent", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=ax3)
    ax3.hlines(true_rpm3, *ax3.get_xlim(),  linewidth=1)
    ax3.set_xlabel('$\\alpha$')
    ax3.set_ylabel('$\\Omega_P$, $\\Omega_F$ (rpm)')

    # add inset figures
    axins1 = inset_locator.zoomed_inset_axes(ax1,
                                            zoom=1.8,
                                            loc=1,
                                            bbox_to_anchor=(0.45, 0.85),
                                            bbox_transform=ax1.transAxes,
                                            borderpad=0)  # zoom = 6

    sns.tsplot(data=df_plot3_rpm1, time="alpha_percent", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=axins1)

    mark_inset(ax1, axins1, loc1a=4, loc1b=4, loc2a=2, loc2b=2, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 0.26, 0.36, 2980, 3180
    axins1.set_xlim(x1, x2)
    axins1.set_ylim(y1, y2)

    axins1.set_xlabel('')
    axins1.set_ylabel('')
    axins1.set_xticks([])
    axins1.set_yticks([])
    axins1.legend_.remove()

    # add another inset figure
    axins2 = inset_locator.zoomed_inset_axes(ax1,
                                            zoom=1.8,
                                            loc=4,
                                            bbox_to_anchor=(0.8, 0.02),
                                            bbox_transform=ax1.transAxes,
                                            borderpad=0)  # zoom = 6

    sns.tsplot(data=df_plot3_rpm1, time="alpha_percent", unit="seed_no", condition="Method", err_style="ci_band",
               value="rpm", estimator=np.nanmean, ax=axins2)

    mark_inset(ax1, axins2, loc1a=1, loc1b=4, loc2a=2, loc2b=3, fc="none", ec="0.5")

    # sub region of the original image
    x1, x2, y1, y2 = 0.2, 0.3, 2400, 2600
    axins2.set_xlim(x1, x2)
    axins2.set_ylim(y1, y2)

    axins2.set_xlabel('')
    axins2.set_ylabel('')
    axins2.set_xticks([])
    axins2.set_yticks([])
    axins2.legend_.remove()

    # # show the figure
    plt.tight_layout(w_pad=0.5, h_pad=0.5)
    plt.savefig('pers_vary_alpha.png', dpi=200)
    plt.show()


# plot_alpha_figs()
# plot_epsilon_figs()

# # now plot the heat maps that show the p-values
# # figure 3, alpha plots
# fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2)
#
# now melt the data
# df_plot4 = x_alpha.rename(columns={"rpm_fourier": "Fourier", "rpm_pers": "Persistence",
#                                           "time_pers": "Persistence Time", "time_fourier": "Fourier Time"})
# df_plot4_melted = pd.melt(df_plot4, value_name="rpm", var_name="Method",
#                           id_vars=['nominal_rpm', 'alpha_percent', 'seed_no'],
#                           value_vars=['Persistence', 'Fourier'])\
#     .groupby(['nominal_rpm', 'Method'])['rpm']\
#     .apply(np.array).reset_index()\
#     .pivot(index='nominal_rpm', columns='Method', values='rpm')
# print(x_alpha.head())
df1 = x_alpha[['alpha_percent', 'eps_percent', 'nominal_rpm', 'seed_no', 'time_pers', 'time_fourier']]
df2 = x_epsilon[['alpha_percent', 'eps_over_T', 'nominal_rpm', 'seed_no', 'time_pers', 'time_fourier']].rename(columns={"eps_over_T": "eps_percent"})

df2.loc[:, 'seed_no'] = np.max(df1['seed_no']) + df2.loc[:, 'seed_no'] + 1

# df_runtime = pd.concat([df1, df2], ignore_index=False, keys=['x', 'y']).reset_index()
df_runtime = pd.concat([df1, df2], ignore_index=False, keys=['x', 'y'])

# print(df_runtime.head())
print(df_runtime.tail(5))

df_runtime = df_runtime.rename(columns={"time_fourier": "Fourier", "time_pers": "Persistence"})

df_runtime_melted = pd.melt(df_runtime, value_name="run time", var_name="Method",
                            id_vars=['nominal_rpm', 'seed_no'],
                            value_vars=['Persistence', 'Fourier']).groupby(['nominal_rpm', 'seed_no', 'Method']).mean().reset_index()

print(df_runtime_melted.head(10))

print(df_runtime_melted.shape)

# df_runtime_melted.set_index(['nominal_rpm', 'seed_no', 'Method'], append=True)
#
# df_plot2_melted = pd.melt(x_epsilon, value_name="rpm", var_name="Method", id_vars=['nominal_rpm', 'eps_over_T', 'seed_no'],
#                           value_vars=['rpm_pers', 'rpm_fourier'])
#
# print(df_plot2_melted.shape)
# print(df_plot2_melted.head())
# print(len(df_plot2_melted['nominal_rpm'].unique()))

# print(df_runtime_melted.head())
# print(df_runtime_melted.tail())
# print(len(df_runtime_melted['nominal_rpm'].unique()))
# print(len(x_epsilon['nominal_rpm'].append(x_epsilon['nominal_rpm']).unique()))
# print(df_runtime_melted.tail())
# print(df_runtime_melted.shape)

# # # plot run time comparisons
fig, ax0 = plt.subplots()
# ax0.get_yaxis().get_major_formatter().set_scientific(True)
# plot subfigure 2
colors = ["black", "grey", "white"]
sns.tsplot(data=df_runtime_melted, time="nominal_rpm", unit="seed_no", condition="Method", err_style="ci_bars",
           value="run time", estimator=np.nanmean, color=['g', 'b', 'm', 'm'], ax=ax0)
ax0.set_xlabel('$\\Omega_0$ (rpm)', fontsize=18)
ax0.set_ylabel('run time (seconds)', fontsize=18)


# # get the xticks labels
texts_ax0_x = [t.get_text() for t in ax0.get_xticklabels()]
texts_ax0_y = [t.get_text() for t in ax0.get_yticklabels()]

ax0.set_xticklabels(labels=texts_ax0_x, fontsize=14)
ax0.set_yticklabels(labels=texts_ax0_y, fontsize=14)
#
# # covert the obtained xticks to floats
# zz_ax0 = np.asarray(texts_ax0, dtype=float)
#
# # format the xticks
# xticks_ax0 = ['{:0.0f}'.format(x) for x in zz_ax0]
# ax0.set_xticklabels(labels=xticks_ax0)
ax0.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%.0e'))
ax0.xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%.0f'))

plt.tight_layout()
plt.savefig('run_time.pdf', dpi=200)

plt.show()
# print(df_plot4_melted.head())
#
# row_ind = 50
# col_ind = 1
# sample = ttest_1samp(a=df_plot4_melted.iloc[row_ind, col_ind], popmean=df_plot4_melted.index[row_ind], nan_policy='omit')
# print(sample)

# for row in range(df_plot4_melted.shape[0]):
#     # print(df_plot4_melted.iloc[row, 1])
#     x = ttest_1samp(a=df_plot4_melted.iloc[row, 1], popmean=df_plot4_melted.index[row],
#                                        nan_policy='omit')
    # print(row, df_plot4_melted.index[row], np.nanmean(df_plot4_melted.iloc[row, 1]), x[1])
    # print(x[1])


# figure 5:
# now plot the heat maps that show the p-values for the computation times
# fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(nrows=2, ncols=2)
#
# print(x_alpha.columns.values)
# print(df_plot4[['Persistence Time', 'Fourier Time']])
# # now melt the data
# x = ttest_rel(a=df_plot4['Persistence Time'], b=df_plot4['Fourier Time'], nan_policy='raise')
# print(x)
#
# print(normaltest(df_plot4['Fourier Time']))
# df_plot5_melted = pd.melt(df_plot4, value_name="time", var_name="Method",
#                           id_vars=['nominal_rpm', 'alpha_percent', 'seed_no'],
#                           value_vars=['Persistence Time', 'Fourier Time'])\
#     .groupby(['nominal_rpm', 'Method'])['time']\
#     .apply(np.array).reset_index()\
#     .pivot(index='nominal_rpm', columns='Method', values='time')
#
# for row in range(df_plot5_melted.shape[0]):
#     # print(df_plot4_melted.iloc[row, 1])
#     normaltest_fourier = normaltest(df_plot5_melted.iloc[row, 0], nan_policy='omit')
#     normaltest_persistence = normaltest(df_plot5_melted.iloc[row, 1], nan_policy='omit')
#
#     # plt.hist(df_plot5_melted.iloc[row, 0])
#     #
#     # plt.show()
#
#     tstat, pvalue = ttest_rel(a=df_plot5_melted.iloc[row, 0], b=df_plot5_melted.iloc[row, 1],
#                                        nan_policy='omit')
#     # print(row, df_plot4_melted.index[row], np.nanmean(df_plot4_melted.iloc[row, 1]), x[1])
#     print(normaltest_fourier[1], normaltest_persistence[1])
#
