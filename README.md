This repo contains the files and the data associated with the paper titled:
**"Topological Data Analysis for True Step Detection in Piecewise Constant 
Signals."**

The data includes:
1. The code used to generate the synthetic data, and analyze the experimental data
2. The .mat files that include the experimental data
3. A file called laser_tach_experiment_info.xlsx which includes information
related to the experiment